# Processing XML With Spark and Zeppelin

Along the journey of trying to process XML data using Zeppelin I came across a neat SerDe that does the trick.

Hive XML SerDe is an XML processing library based on Hive SerDe (serializer / deserializer) framework. It relies on XmlInputFormat from Apache Mahout project to shred the input file into XML fragments based on specific start and end tags.

Using this lirary allows a hive table to be created on top of raw XML files and makes quering the XML possible. It also offers a great deal of flexability over which tags need to be included. What I love about it is that it parses the XML on the fly as you query the table.

To install the library on a Spark Cluster, ssh to the cluster and run the following.

<pre><code class="bash"># Create the directory of the jar file
mkdir /home/hive/jars
cd /home/hive/jars
# Download the jar file
wget http://search.maven.org/remotecontent?filepath=com/ibm/spss/hive/serde2/xml/hivexmlserde/1.0.5.3/hivexmlserde-1.0.5.3-sources.jar
# Rename the jar file for more convenience
mv remotecontent?filepath=com/ibm/spss/hive/serde2/xml/hivexmlserde/1.0.5.3/hivexmlserde-1.0.5.3-sources.jar hivexmlserde-1.0.5.3-sources.jar

</code></pre>

The next step is to configure Zeppelin. Click on the drop down next to anonymous and select interpreter.

![zeppelin1][1]

Scroll down an click edit to edit the Spark interpreter. Scroll down untill the Depencies section is visiable. Add the path to the Jar file as an atifact.

![zeppelin2][2]

Now we need to create a table that uses the library. In the following example we are using XML from tb_UserGameData.

<pre><code class="python">%pyspark

sqlContext.sql("""drop table if exists gamemonitoring.tb_XMLPlayerInfo""")


sqlContext.sql("""

CREATE EXTERNAL TABLE tb_XMLUserInfo(
       userId string,
       sessionId string,
       balance string,
       currencyId string,
       userType string

     )
     ROW FORMAT SERDE 'com.ibm.spss.hive.serde2.xml.XmlSerDe'
     WITH SERDEPROPERTIES (
     "column.xpath.userId"="UserInfo/@userId",
     "column.xpath.transNumber"="UserInfo/@sessionId",
     "column.xpath.balance"="UserInfo/@balance",
     "column.xpath.currencyId"="UserInfo/@currencyId",
     "column.xpath.userType"="UserInfo/@userType"
     )
     STORED AS
     INPUTFORMAT 'com.ibm.spss.hive.serde2.xml.XmlInputFormat'
     OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.IgnoreKeyTextOutputFormat'
     LOCATION 'wasb://staging@fictitiousstorage.blob.core.windows.net/xml'
     TBLPROPERTIES (
     "xmlinput.start"="&lt;UserInfo",
     "xmlinput.end"="&lt;/UserInfo&gt;"
     )
     """)

sqlContext.sql("""select * from tb_XMLUserInfo""").show(300,False)

</code></pre>

To see this in action upload some XML data to a wasb or S3 location and make sure its specified in the create table statement.

<pre><code class="xml">&lt;XML&gt;
&lt;X version="133.6.0"&gt;
&lt;GameFramework stateId="0"&gt;
&lt;UserInfo balance="811140" currencyId="0" userId="1839" userType="0" sessionId="110"&gt;
&lt;OtherInfo pagesVisited="500" firstPage="6" clickThrough="9"/&gt;
&lt;/UserInfo&gt;
&lt;/XML&gt;
</code></pre>

The Spark-XML library also allows for the shreading of XML using SparkSQL and Dataframes. It runs easily in Zeppelin.

In order to get the library working it is neccessary to create a depency. This is done as follows:

<pre><code class="python">%dep
z.load("com.databricks:spark-xml_2.11:0.4.1")

</code></pre>

After that querying the XML is as easy as:

<pre><code class="python">%pyspark
sqlContext.read.format("com.databricks.spark.xml").option("rowTag", "PlayerInfo").load("wasb://staging@gamemonitoringstorage.blob.core.windows.net/xml").show()
</code></pre>

 [1]:  https://bytebucket.org/beauzec/beautec/raw/7c3429fca11f84002eda6b3489e2346a4871834b/Processing%20XML%20With%20Spark%20and%20Zeppelin/images/zeppelin01.jpg
 [2]: https://bytebucket.org/beauzec/beautec/raw/7c3429fca11f84002eda6b3489e2346a4871834b/Processing%20XML%20With%20Spark%20and%20Zeppelin/images/zeppelin02.jpg

