# Real Time Monitoring With Grafana and Influx DB

Grafna and Influx DB together make an extreemly powerfull real time monitoring tool thats looks beautiful and gets the job done. Setting everything up is extreemly simple just head over to https://grafana.com/ and https://portal.influxdata.com/downloads respectively and download the latest versions for whichever platform you are on. 

Grafana works with many Datasources but Influx DB is one of my favourite. It's easy to install, configure and get running in no time at all.  Grafana sits on top of InfluxDB so as metrics are pushed to InfluxDB they appear as awesome time series graphs in Grafana.

![grafana][1]

## Sending data to InfluxDB using PowerShell and T-SQL
We had a scenario where we needed to monitor SQL Server Database Mirroring close to realtime as the data travesered the Atlatic Ocean from one continant to another for Disaster Recovery. In order to do this with the least amount of effort we spun up an EC2 instance and installed InfluxDB and Grafana.

The next step was to write some code to get the Database Mirroring Metrics from the servers and send them to InfluxDB.

We chose to make use of a SQL Job that executed a PowerShell script. Something like this:

```powershell
$uri = 'http://<ip-address>:8086/write?db=DR'

$SQLQuery = "
SET NOCOUNT ON
DECLARE @MirrorResults table (database_name varchar(100),role int,mirroring_state int,witness_status int,log_generation_rate int,unsent_log int,send_rate int, unrestored_log int,recovery_rate int,
transaction_delay int,transactions_per_sec int,average_delay int,time_recorded datetime,time_behind datetime, local_time datetime)

Insert @MirrorResults exec msdb.dbo.sp_dbmmonitorresults mirrortest,0,0

If (Select DateDiff(minute,Time_Behind,time_recorded) from @MirrorResults) > 10 
Update @MirrorResults Set mirroring_state = 5

Select database_name
,isnull(log_generation_rate,0)log_generation_rate
,isnull(unsent_log,0)[unsent_log]
,isnull(send_rate,0)[send_rate]
,isnull(unrestored_log,0)[unrestored_log]
,isnull(recovery_rate,0)[recovery_rate]
,isnull(transaction_delay,0)[transaction_delay]
,isnull(transactions_per_sec,0)[transactions_per_sec]
,isnull(average_delay,0)[average_delay]
,isnull(DateDiff(second,Time_Behind,time_recorded),0)[mirror_lag_sec] 
from @MirrorResults"


$sqlresults = Invoke-Sqlcmd -Query $SQLQuery -ServerInstance "localhost" 
foreach ($result in $sqlresults) 
{
# This lets us pick out each instance ($inst) and database ($name) as we iterate
# through each pair of server / database.


$database_name = $result.database_name 
$log_generation_rate = $result.log_generation_rate 
$unsent_log = $result.unsent_log 
$send_rate = $result.send_rate 
$unrestored_log = $result.unrestored_log 
$recovery_rate = $result.recovery_rate 
$transaction_delay = $result.transaction_delay 
$transactions_per_sec = $result.transactions_per_sec 
$average_delay = $result.average_delay 
$mirror_lag_sec = $result.mirror_lag_sec


$body = 'MirroringMonitoring,database_name=' + $database_name + ',log_generation_rate=' + $log_generation_rate + ',unsent_log=' + $unsent_log + ',send_rate=' + $send_rate + ',unrestored_log=' + $unrestored_log+ ',recovery_rate=' + $recovery_rate+ ',transaction_delay=' + $transaction_delay+ ',transactions_per_sec=' + $transactions_per_sec + ',average_delay=' + $average_delay + ' mirror_lag_sec=' + $mirror_lag_sec
echo $body
Invoke-RestMethod -Method Post -Uri $uri -Body $body
```

The above script queries SQL Server for some metrics and then sends them off to InfluxDB. 

## Sending data example using Python

Sending data using Python is very striaght forward. Here is a sample script that sends Random values for testing.

```python

import requests
import json


from random import randint
randomnumber = (randint(0,9))


strdata = "pythondata,host=server01,region=us-west value=" + str(randomnumber)
print (strdata)
payload = strdata
requests.post("http://<ip-address>:8086/write?db=sqlmetrics", data=(payload),verify=False)

```


 [1]:   https://bytebucket.org/beauzec/beautec/raw/b40f2a1fce6b2c123f1eefb2c3199da8aa0e83ae/Real%20Time%20Monitoring%20With%20Graffana%20And%20Influx%20DB/images/Grafana1.jpg








