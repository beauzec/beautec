# Real Time Monitoring with Dashing

Monitoring has always been a passion of mine and knowing how to incorportare real-time monitoring into your projects gives insights into how things are performing and over time you start to learn what is normal and when something odd is occuring. 

My favoutite set of tools for doing this quickly and on shoestring budget are Dashing, Graffana and InfluxDB.

## Dashing

Dashing was created by some clever guys over at shopyfy years back and whilst there are many other alternativities I have always come back to Dashing. It's simple, looks great and it just works. It also works with autentication!

It's only limitation and it's no fault of dashings is that it was desinged to work with server side events. IE does not support server side events so it will work with every other browser. Just not IE. I have come accross a company where there was a policy to use only IE so Dashing was not an option.

There is plenty of info on installing dashing and adding widgets etc so I am just going to run through a brief install guide so we can get to the fun stuff!

Installing Dashing is straight forward and can be done in 3 steps! 

1) Dashing runs on Ruby so you need to install Ruby

    ``` $ sudo apt-get install ruby1.9.1-dev ```

2) Install g++. This is a GNU Complier collection. 

    ```$ sudo apt-get install g++```

3) Install a javascript runtime.

    ```$ sudo apt-get install npm```

4) Once Ruby is installed Dashing gets installed as a Gem

    ```$ sudo gem install dashing```

5) Last step is to install the Bundler Gem whichj is neccessary to bundle a new Dashboard together.

    ```$ sudo gem install bundler```


6) Now its time to create a sample dashboard. It is as easy as

    ```$ dashing new sample-dashboard ```

7) To start Dashing use the command:

    ```$ dashing start```

8) By default dashing listens on port 3030 so browse to you sample dashing site.

    ```http//:127.0.0.1:3030```

Hopefully you see this:

![dashing1][2]

I tend to run Dashing on in AWS on t2 micro instances. Cheap as chips and it works beautifully on them. T2 micros are also free tier eligible.

Now the real fun starts:

Here are some examples of different ways you can get key data to Dashing

## Sending Data to Dashing With T-SQL

Before we begin connect to SQL with SSMS and ensure that Ole Automation Procedures are enabled. If they are not enabled you will get the following error message.
```
%sql Msg 15281, Level 16, State 1, Procedure sp_OACreate, Line 1 [Batch Start Line 0]
SQL Server blocked access to procedure 'sys.sp_OACreate' of component 'Ole Automation Procedures' because this component is turned off as part of the security configuration for this server. A system administrator can enable the use of 'Ole Automation Procedures' by using sp_configure. For more information about enabling 'Ole Automation Procedures', search for 'Ole Automation Procedures' in SQL Server Books Online.
Msg 15281, Level 16, State 1, Procedure sp_OAMethod, Line 1 [Batch Start Line 0]
```

To see if they are enabled run

```sql 
Exec sp_configure 'Show Advanced Options',1
Exec sp_configure 'Ole Automation Procedure'
```

If they are not enabled run:

```sql
Exec sp_configure 'Ole Automation Procedures',1
Reconfigure
```

Now you are ready to experiment! Try running the following in SSMS.

```sql
Declare @Object as Int;
Declare @ResponseText as Varchar(8000);
Declare @Body as varchar(8000) = '{ "auth_token": "YOUR_AUTH_TOKEN", "text": "Works like a boss" }'  

Exec sp_OACreate 'MSXML2.ServerXMLHTTP', @Object OUT;
EXEC  sp_OAMethod @Object, 'open', NULL, 'post','http://<ip-address>:3030/widgets/welcome', 'false'

Exec sp_OAMethod @Object, 'setRequestHeader', null, 'Content-Type', 'application/json'
Exec sp_OAMethod @Object, 'send', null, @body

Exec sp_OAMethod @Object, 'responseText', @ResponseText OUTPUT
Select @ResponseText

Exec sp_OADestroy @Object
```

It is super easy to send data stright from SQL to Dashning.

## Sending Data to Dashing With PowerShell

When it comes to sending data using PowerShell it even gets easier

```powershell
$authToken = "YOUR_AUTH_TOKEN"
$uri = "http://<ip-address>:3030/widgets/welcome"
$text = "Whats Happening!"
$Payload = @{auth_token=$authToken;text=$text}
Invoke-RestMethod -Uri $uri -Method Post -ContentType 'application/json' -Body (ConvertTo-Json $Payload)
```

## Sending data to Dashing With Python

Python has become my favourite scripting language. It's easy flexible and can pretty much do everything I need to do. Here is an example of sending messages to Dashing with Python.

```python
import requests            #conda install requests
import simplejson as json  #conda install simplejson

url = "http://<ip-address>:3030/widgets/welcome"
widget = "welcome"
data = { "auth_token": "YOUR_AUTH_TOKEN", "text": "Well hello there!" }

headers = {'Content-type': 'application/json'}
requests.post(url, data=json.dumps(data), headers=headers)
```

Sending data to dashing is extreemly simple and makes building dashboards like this one extreemly easy. The key is to be always thinking about monitoring when orchestrating big complex solutions so that there is always visibiility into how things are running.

![dashing2][1]

 [1]:   https://bytebucket.org/beauzec/beautec/raw/f663894c06dc2549ded6d824472969e74a73caf5/Real%20Time%20Monitoring%20With%20InfluxDB%20and%20Graffana/images/dashing1.jpg
 [2]: https://bytebucket.org/beauzec/beautec/raw/f663894c06dc2549ded6d824472969e74a73caf5/Real%20Time%20Monitoring%20With%20InfluxDB%20and%20Graffana/images/dashing2.jpg




















