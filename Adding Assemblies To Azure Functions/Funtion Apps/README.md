# Creating a Resume and Pause Function App

There are numerous cases where we don't need to have an Azure Analysis Server up and running 24/7.
In order to achive this I set out to create an HTTP Trigger Azure Function App to take care of the stoping and starting of the Analysis Services Instance. I thought doing this would this would be easy but I wasn't able to find anything that just detailed how to do this and it prooved to be a bit tricky in places. I documented the process of how to create Azure Functions to pause and resume an Azure Analysis Server using Powershell. Something I learned is that before we can get Powershell working with Azure Functions some additional modules are required and this will hopefully save you the pain and frustration I went through to figure this out. 



## Create a Function APP
The first thing we need to do is create a new Function App. If you are unsure of how to do this here is great article on how to this:

https://docs.microsoft.com/en-us/azure/azure-functions/functions-create-first-azure-function


## Application Registration

To tie the Function App to Analysis services we are going to register an app and grant permissions to the app in Analysis Services. We are then going to tell our function to use the app in our function so our function will have the required access. There are 3 critical pieces of information obtained in this step that will need later.

Registring an app is easy. To register a new app, open up Azure Acive Directy and select App Registrations. 

Give the app a name, select Web app/API and the in the Sign-on-URL you can put anything. I tend to use https://portal.azure.com.

Now naviagte to Azure Analysis Services and select Access control (IAM), then select Add and in the Add Permissions tile that appears on the right fill in the details. 

* The Role needs to be Contributor.
* In the Assign access to field select "Azure AD user, group, or application".
* In the select field start typing the name of the App you created and it should appear.
* Click on save and you are done.

Now that the app has been saved we need the following:

$AppID = The ID of the App you created in Azure Active Directory

To get this click on Azure Active Directory and click on the app you just created and copy the Application ID.

$SecretKey = The Key of the App you create in Azure Active Directory

You will need to create a key, sp clck on settings Keys. Specify a descrition, select an expirey option and hit save. Once you save it the key will be revealed. Copy this for later.

 $TenantID = Your Active Directory ID
 
 The Directory ID that can be found by selecting Properties in Azure Active Directory. Save this too for later


## Creating the Suspend and Resume Function

Now we need to create a function within the Function App so navigate to the Function App you created. Click on the + next to Functions, then select "create your own custom function"

![1]

On the next screen select "Enable Experimental Lanuguage Support" and choose PowerShell as the Language. At the time of writing this, you should now be presented with 3 options. Select HTTP Trigger.

![2]

Give your function a name and click Create.

![3]


## Configuring the Function Environment

Now we need to configure the function and this is going to invole a bit of code and cofig. We are going to install an updated version of the Powershell module AzureRM.Profile which will allow us to install the module AzureRM.AnalysisServices. The process will be to download the latest version of this module and upload it to a directory in your function so we can reference it later. We will add a block of code into our function that will check if this module is installed and install it if neccessary. I did try installing the module via the Powershell Console in the Kudo app to see if it would persist but it doesn't. When a Function App gets restarted all custom installed modules are cleared.

To grab the latest version of AzureRM.Profile open up Powershell on your local machine and run  the following command to download it:

```Powershell
Save-Module -Name AzureRM.Profile -Path "C:\temp\"
```

Once it has completed downloading we will need to upload the module to our Function environment. To do this we are going to use the Kudo Console. 

To open the Kudo Console, click on your function app, select Platform features and select Advanced tools (Kudu). 

![5]

Click on the Debug Console Menu and select CMD. Navigate to D:\home\site\wwwroot\<Function Name> and create a new folder for the Powershell modules. I have called mine azuremodules.

![6]

Click on the folder azuremodule and drag the contents of the module you downloaded to azuremodules. 

![7]

One last thing to do is to increase the time-out for the function app so it installs the modules before it times out. To do this, click on the Function App, and then the App Service Editor. When it opens click on host.json and add the following. This will set the time-out to 10 minutes which is the max. As you change anything in the App Service Editor it gets saved automaticly.

```json
{"functionTimeout": "00:10:00"}
```
![8]

Thats it! The function environment is configured. 


## Adding the Powershell Code to the Function

The pause and resume function expects 3 parametrs to be passed in the body so it knows if it needs to resume or suspend the analysis server. It also needs to know the resource group and the server name. Here is an example:

```json
{
    "action": "Suspend",
    "resourceGroup":"Beautec",
    "vmName":"andretest"
}
```

The first bit of code that needs to be added to the function is the configuration code that will install the modules if they are missing. We added the Powershell modules in the previous steps, now we need to make sure they are installed. 
```Powershell
#======================================================================================================================
#Configuration
#======================================================================================================================

Write-Output "PowerShell Timer trigger function executed at:$(get-date)";

#Get Details about the AzureRM.Profile Module. 
$module = Get-Module -ListAvailable -Name "AzureRM.Profile" 
$numModules = $module.version.count

#There should be two versions. If they arn't install it
if ($numModules -ne "2") {
    Write-Output "The latest version of AzureRM.Profile not installed";
    Write-Output "Installing AzureRM.Profile";
    Install-Module -Name AzureRM.Profile -Scope CurrentUser -ErrorAction Continue -Verbose -force;
    Echo "Finished installing, checking if successful";
    $module = Get-Module -ListAvailable -Name "AzureRM.Profile" 
    $numModules = $module.version.count
}


if ($numModules -ne "2") {
    Write-Output "Failed to install AzureRM.Profile";
}

if ($numModules -eq "2") {
    Write-Output "AzureRM.Profile is installed";
}



if (-not (Get-Module -listavailable -Name "AzureRM.AnalysisServices")) {
    Write-Output "AzureRM.AnalysisServices not installed";
    Write-Output "Installing AzureRM.AnalysisServices";
    Get-PackageProvider -Name nuget -ForceBootstrap;
    Install-Module -Name AzureRM.AnalysisServices -AllowClobber -Scope CurrentUser -force;
    Write-Output "Finished installing, checking if successful";
}

if (-not (Get-Module -listavailable -Name "AzureRM.AnalysisServices")){
    Write-Output "Failed to install AzureRM.AnalysisServices";
}

if (Get-Module -listavailable -Name "AzureRM.AnalysisServices") {
    Write-Output "AzureRM.AnalysisServices is Installed";
}

```

Next we need to Authenticate with our Azure Analysis Server. If you havn't configured an app to authenticate with your Analysis Server yet here is a link to do so. You will need to update the section Set our keys with the information aquired when configuring access. 

$AppID = The ID of the App you created in Azure Active Directory
$SecretKey = The Key of the App you create in Azure Active Directory
$TenantID = The Directory ID that can be found by selecting Properties in Azure Active Directory.

```Powershell

#======================================================================================================================
# Authenticate
#======================================================================================================================

#Set Our Keys
$AppID = <YourAppID>
$SecretKey = <YourSecret>
$TenantID = <YourTenantID>


$secpasswd = ConvertTo-SecureString $SecretKey -AsPlainText -Force 
$mycreds = New-Object System.Management.Automation.PSCredential ($AppID, $secpasswd) 
$result = Login-AzureRmAccount -ServicePrincipal -Tenant $TenantID -Credential $mycreds 
```

Now that we have autheticated we can just call the commands to either suspend or resume the server.

```PowerShell
#======================================================================================================================
# Send suspend or resume request
#======================================================================================================================

# Suspend
if ($action -eq 'Suspend') 
{
    Suspend-AzureRmAnalysisServicesServer -Name $vmName -ResourceGroupName $resourceGroup;
    Out-File -Encoding Ascii -FilePath $res -inputObject "Suspending server";
}


# Resume
if ($action -eq 'Resume') 
{
    Resume-AzureRmAnalysisServicesServer -Name $vmName -ResourceGroupName $resourceGroup;
    Out-File -Encoding Ascii -FilePath $res -inputObject "Resuming server";
}

```


Here is the complete Powershell code:

```Powershell
#======================================================================================================================
#Configuration
#======================================================================================================================

Write-Output "PowerShell Timer trigger function executed at:$(get-date)";

#Get Details about the AzureRM.Profile Module. 
$module = Get-Module -ListAvailable -Name "AzureRM.Profile" 
$numModules = $module.version.count

#There should be two versions. If they arn't install it
if ($numModules -ne "2") {
    Write-Output "The latest version of AzureRM.Profile not installed";
    Write-Output "Installing AzureRM.Profile";
    Install-Module -Name AzureRM.Profile -Scope CurrentUser -ErrorAction Continue -Verbose -force;
    Echo "Finished installing, checking if successful";
    $module = Get-Module -ListAvailable -Name "AzureRM.Profile" 
    $numModules = $module.version.count
}


if ($numModules -ne "2") {
    Write-Output "Failed to install AzureRM.Profile";
}

if ($numModules -eq "2") {
    Write-Output "AzureRM.Profile is installed";
}



if (-not (Get-Module -listavailable -Name "AzureRM.AnalysisServices")) {
    Write-Output "AzureRM.AnalysisServices not installed";
    Write-Output "Installing AzureRM.AnalysisServices";
    Get-PackageProvider -Name nuget -ForceBootstrap;
    Install-Module -Name AzureRM.AnalysisServices -AllowClobber -Scope CurrentUser -force;
    Write-Output "Finished installing, checking if successful";
}

if (-not (Get-Module -listavailable -Name "AzureRM.AnalysisServices")){
    Write-Output "Failed to install AzureRM.AnalysisServices";
}

if (Get-Module -listavailable -Name "AzureRM.AnalysisServices") {
    Write-Output "AzureRM.AnalysisServices is Installed";
}




#======================================================================================================================
# POST method: $req
#======================================================================================================================
$requestBody = Get-Content $req -Raw | ConvertFrom-Json
$action = $requestBody.action
$resourceGroup = $requestBody.resourceGroup
$vmName = $requestBody.vmName


#======================================================================================================================
# Authenticate
#======================================================================================================================

#Set Our Keys
$AppID = <YourAppID>
$SecretKey = <YourSecret>
$TenantID = <YourTenantID>


$secpasswd = ConvertTo-SecureString $SecretKey -AsPlainText -Force 
$mycreds = New-Object System.Management.Automation.PSCredential ($AppID, $secpasswd) 
$result = Login-AzureRmAccount -ServicePrincipal -Tenant $TenantID -Credential $mycreds 



#======================================================================================================================
# Send suspend or resume request
#======================================================================================================================

# Suspend
if ($action -eq 'Suspend') 
{
    Suspend-AzureRmAnalysisServicesServer -Name $vmName -ResourceGroupName $resourceGroup;
    Out-File -Encoding Ascii -FilePath $res -inputObject "Suspending server";
}


# Resume
if ($action -eq 'Resume') 
{
    Resume-AzureRmAnalysisServicesServer -Name $vmName -ResourceGroupName $resourceGroup;
    Out-File -Encoding Ascii -FilePath $res -inputObject "Resuming server";
}





```

Thats it. You now have a HTTP Trigget Function that can be called by anything that can stop and start Analysis Services. 

[1]:./images/functionapp1.jpg
[2]:./images/functionapp2.jpg
[3]:./images/functionapp3.jpg
[4]:./images/functionapp4.jpg
[5]:./images/functionapp5.jpg
[6]:./images/functionapp6.jpg
[7]:./images/functionapp7.jpg
[8]:./images/functionapp8.jpg










